//
//  IPKTools.swift
//  IPKCommon
//
//  Created by Noel Miguel Martinez Cadena on 30/07/18.
//  Copyright © 2018 Noel Miguel Martinez Cadena. All rights reserved.
//

public class BAVTools: NSObject {
    public class func MakeError(message: String) -> NSError {
        var errorDetail: [String : Any] = [:]
        
        errorDetail[NSLocalizedDescriptionKey] = message
        
        return NSError(domain: "com.bancomer.bbva.BAVCommon", code: 121086, userInfo: errorDetail)
    }
}
