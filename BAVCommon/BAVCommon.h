//
//  BAVCommon.h
//  BAVCommon
//
//  Created by Noel Miguel Martinez Cadena on 30/07/18.
//  Copyright © 2018 Noel Miguel Martinez Cadena. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BAVCommon.
FOUNDATION_EXPORT double BAVCommonVersionNumber;

//! Project version string for BAVCommon.
FOUNDATION_EXPORT const unsigned char BAVCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BAVCommon/PublicHeader.h>


